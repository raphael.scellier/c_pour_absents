#include <stdio.h>
#include <stdlib.h>

typedef struct liste
{
    int info;
    struct list *svt;
}list,*plist;

//A-2-a : decouplage donn�e/sortie

plist insertion1(plist tete,int elt)
{
    plist aux;
    aux = (plist)malloc(sizeof(list));
    aux->info=elt;
    aux->svt=tete;
    return aux;
}

void insertion2(plist*tete,int elt)//A-2-b
{

    plist aux;
    aux = (plist)malloc(sizeof(list));
    aux->info=elt;
    aux->svt=*tete;
    *tete=aux;
}

plist creation()//A-4
{
    plist tete=NULL;
    int elt,choix;
    do
    {
        printf("entrer elt\n");
        scanf("%d",&elt);
        tete = insertion1(tete,elt);
        //ou insertion2(&tete,elt);
            /*OU Alors
               tete=insertionfin1(tete,elt);
               ou
               insertionfin2(&tete,elt);
            */
        printf("encore insertion ? [1/0]\n");
        scanf("%d",&choix);
    }while(choix==1);
    return tete;
}

void affiche(plist tete)// A-5
{
    while(tete!=NULL)
    {
        printf("%d\n",tete->info);
        tete=tete->svt;
    }
}

plist insertionfin1(plist tete, int elt)
{
    plist aux,p;
    aux=(plist)malloc(sizeof(list));
    aux->info=elt;
    aux->svt=NULL;
    if(tete==NULL) return aux;
    //Facultatif
    /*else
    {
        p=tete;
        while(p->svt!=NULL)
            p=p->svt;
        p->svt=aux;
        return tete;
    }
    */
    return tete; //Enlever si il y a le else
}

void insertionfin2(plist * tete,int elt)
{
    plist aux,p;
    aux=(plist)malloc(sizeof(list));
    aux->info=elt,
    aux->svt=NULL;
    if(*tete==NULL)
        *tete=aux;
    else
    {
        p=*tete;
        while(p->svt!=NULL)
            p=p->svt;
        p->svt=aux;
    }
}

void suppression(plist*tete,int elt)
{
    plist prec,p;
    if(*tete!=NULL)
    {
        p=*tete;
        while((p!=NULL)&&(p->info!=elt))
        {
            prec=p;
            p=p->svt;
        }
        if(p!=NULL)
        {
            if(p==*tete)
            {
                *tete=(*tete)->svt;
                free(p);
            }
            else
            {
                prec->svt=p->svt;
                free(p);
            }
        }
    }
}

void suppressionmulti(plist*tete,int elt)
{

    int drap;
    do
    {
        drap=suppression(&(*tete),elt);
    }
    while(drap==1);//while(drap);
}

int main()
{
    plist tete=NULL;
    tete = creation();
    affiche(tete);
    return 0;
}
